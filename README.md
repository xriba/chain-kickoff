# chain-kickoff
### Prerequesities
ubuntu 22.10

## Getting started
Download kickoff.sh
```
curl -LJO https://gitlab.com/xriba/chain-kickoff/-/raw/master/scripts/kickoff.sh
chmod +x kickoff.sh
```
Make sure to give the script executable permissions (chmod +x kickoff.sh) before running it.

## Prepare your node
run ```./kickoff.sh```

## Clean up
To uninstall and delete files download cleanup.sh
run ```./cleanup.sh```

```
curl -LJO https://gitlab.com/xriba/chain-kickoff/-/raw/master/scripts/cleanup.sh
chmod +x cleanup.sh
```

### only some files:
rm -rf node/data/keystore
sudo docker rm node

sudo rm -rf node/data
