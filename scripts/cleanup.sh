#!/bin/bash

# Stop and remove Docker container
sudo docker stop $(sudo docker ps -q)
sudo docker rm $(sudo docker ps -a -q)
sudo docker rmi $(sudo docker images -q)

# Remove Node.js and Docker
sudo apt-get remove -y docker.io nodejs
sudo apt-get autoremove -y
sudo apt-get autoclean -y
sudo apt-get clean -y

# Remove files
sudo rm -rf node
